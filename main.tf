# new comment
provider "aws" {
  region = "eu-central-1"
}

# create full network
resource "aws_vpc" "actpro-vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "ActPRO-NET"
  }
}

# create front network
resource "aws_subnet" "front-end-net" {
  vpc_id     = aws_vpc.actpro-vpc.id
  cidr_block = "10.0.1.0/24"
  tags = {
    Name = "front-net"
  }
}

# create backend network
resource "aws_subnet" "back-end-net" {
  vpc_id     = aws_vpc.actpro-vpc.id
  cidr_block = "10.0.2.0/24"
  tags = {
    Name = "back-net"
  }
}

# create internet-gateway
resource "aws_internet_gateway" "actpro-gw" {
  vpc_id = aws_vpc.actpro-vpc.id
  tags = {
    Name = "actpro-gw"
  }
}

# create default route via front network
resource "aws_route_table" "actpro-rt-front" {
  vpc_id = aws_vpc.actpro-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.actpro-gw.id
  }
  tags = {
    Name = "actpro-rt-front"
  }
}

# create default route via backend network
resource "aws_route_table" "actpro-rt-back" {
  vpc_id = aws_vpc.actpro-vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.actproNatGW.id
  }
  tags = {
    Name = "actpro-rt-back"
  }
}

# associate public subnet to public route table_front
resource "aws_route_table_association" "a-front-net" {
  subnet_id      = aws_subnet.front-end-net.id
  route_table_id = aws_route_table.actpro-rt-front.id
}

# associate private subnet to private route table_backend
resource "aws_route_table_association" "a-back-net" {
  subnet_id      = aws_subnet.back-end-net.id
  route_table_id = aws_route_table.actpro-rt-back.id
}

# create public NAT
resource "aws_nat_gateway" "actproNatGW" {
  allocation_id = aws_eip.demoEIP.id
  subnet_id     = aws_subnet.front-end-net.id
  tags = {
    Name = "actpro_gw_NAT"
  }
}

# create Elastic IP for GW
resource "aws_eip" "demoEIP" {
  vpc = true
}


# create SG
resource "aws_security_group" "actpro-sg" {
  name        = "ssh-web"
  description = "Allow 22 and 80 ports traffic"
  vpc_id      = aws_vpc.actpro-vpc.id

  # rules
  dynamic "ingress" {
    for_each = ["22", "80", "443"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "ssh-web-sg"
  }
}

# create NACLs
resource "aws_network_acl" "front_nacl_main" {
  vpc_id = aws_vpc.actpro-vpc.id

  # rules
  ingress {
    protocol   = "tcp"
    rule_no    = 50
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 22
    to_port    = 22
  }

  egress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 22
    to_port    = 22
  }

  egress {
    protocol   = "tcp"
    rule_no    = 200
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 32768
    to_port    = 61000
  }

  tags = {
    Name = "front-nacl"
  }
}

# outloop latest AMI
data "aws_ami" "ubuntu-latest" {
  owners      = ["099720109477"]
  most_recent = true # latest
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }
}

# create instance front
resource "aws_instance" "web-server-front" {
  ami                         = data.aws_ami.ubuntu-latest.id
  instance_type               = "t2.micro"
  subnet_id                   = aws_subnet.front-end-net.id
  private_ip                  = "10.0.1.12"
  vpc_security_group_ids      = [aws_security_group.actpro-sg.id]
  associate_public_ip_address = true
  user_data                   = <<EOF
#!/bin/bash
apt update -y
apt install nginx -y
EOF
  key_name                    = "aws_ssh"
  tags = {
    Name = "web-server-front"
  }
}

# create instance backend
resource "aws_instance" "web-server-back" {
  ami                         = data.aws_ami.ubuntu-latest.id
  instance_type               = "t2.micro"
  subnet_id                   = aws_subnet.back-end-net.id
  private_ip                  = "10.0.2.12"
  vpc_security_group_ids      = [aws_security_group.actpro-sg.id]
  associate_public_ip_address = false
  key_name                    = "aws_ssh"
  tags = {
    Name = "web-server-back"
  }
}

# create instance jump-host
resource "aws_instance" "bastion-01_server" {
  ami                         = data.aws_ami.ubuntu-latest.id
  instance_type               = "t2.micro"
  subnet_id                   = aws_subnet.front-end-net.id
  private_ip                  = "10.0.1.100"
  vpc_security_group_ids      = [aws_security_group.actpro-sg.id]
  associate_public_ip_address = true
  key_name                    = "aws_ssh"
  tags = {
    Name = "bastion-01"
  }
}

# output public IP front
output "instance_public_ip_front" {
  description = "Public IP address of the front instance"
  value       = aws_instance.web-server-front.public_ip
}

# output public IP backend
output "instance_public_ip_bastion" {
  description = "Public IP address of the bastion instance"
  value       = aws_instance.bastion-01_server.public_ip
}
